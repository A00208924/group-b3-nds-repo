import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CustSearchForm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustSearchForm frame = new CustSearchForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void NewScreen8() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustSearchForm frame = new CustSearchForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CustSearchForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 534, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("Id:");
		lblId.setFont(new Font("宋体", Font.PLAIN, 13));
		lblId.setBounds(91, 98, 81, 15);
		contentPane.add(lblId);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(182, 95, 96, 21);
		contentPane.add(textField);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("宋体", Font.PLAIN, 13));
		lblName.setBounds(91, 123, 81, 15);
		contentPane.add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(182, 120, 96, 21);
		contentPane.add(textField_1);
		
		JLabel lblSearchcustomer = new JLabel("Search Customer");
		lblSearchcustomer.setFont(new Font("宋体", Font.PLAIN, 16));
		lblSearchcustomer.setBounds(135, 10, 244, 49);
		contentPane.add(lblSearchcustomer);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSearch.setFont(new Font("宋体", Font.PLAIN, 13));
		btnSearch.setBounds(57, 246, 115, 32);
		contentPane.add(btnSearch);
		
		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				CustChoose nw=new CustChoose();
//				nw.NewScreen4();
			}
		});
		button_1.setFont(new Font("宋体", Font.PLAIN, 13));
		button_1.setBounds(249, 246, 115, 32);
		contentPane.add(button_1);
	}
}
