import java.sql.Statement;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.Types;

public class DatabaseConnector {
	String cmd = null;
	Connection con = null;
	Statement stmt = null;
	CallableStatement cStmt = null;
	ResultSet rs = null;
	
	public DatabaseConnector() throws Exception {
		initiate_db_conn();
	}
	
	public void initiate_db_conn() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url="jdbc:mysql://localhost:3306/newspaper_delivery_system?useSSL=no";
		
		con = DriverManager.getConnection(url, "root", "<a password>");
		stmt = con.createStatement();
	}
	
	public boolean createCustomerInDatabase(
			String lastName,
			String otherNames,
			String dateOfBirth,
			String phoneNumber,
			String address,
			String region,
			String deliveryFrequency
	) throws Exception {
		String sqlStr = "";
		int result = 0;
		
		if (lastName != "" && otherNames != "" && dateOfBirth != "" && phoneNumber != "" && address != "" && region != "" && (deliveryFrequency == "daily" || deliveryFrequency == "weekly" || deliveryFrequency == "monthly")) {
			sqlStr = "" +
					"insert into customers (" +
					"  last_name," + 
					"  other_names," + 
					"  date_of_birth," + 
					"  phone_number," + 
					"  address," + 
					"  region," + 
					"  delivery_frequency" +
					") values (" + 
					"  '" + lastName + "'," +
					"  '" + otherNames + "'," +
					"  '" + dateOfBirth + "'," +
					"  '" + phoneNumber + "'," +
					"  '" + address + "'," +
					"  '" + region + "'," +
					"  '" + deliveryFrequency + "'" +
					");"
			;
			
			result = stmt.executeUpdate(sqlStr);
			
			if (result >= 1) return true;
			else return false;
		} else {
			return false;
		}
	}
	
	public boolean createRegionInDatabase(String regionName) throws Exception {
		String sqlStr = "";
		int result = 0;
		
		if (regionName != "") {
			sqlStr = "" +
					"insert into regions (" + 
					"  region_name" +
					") values (" + 
					"  '" + regionName + "'" +
					");"
			;
			
			result = stmt.executeUpdate(sqlStr);
			
			if (result >= 1) return true;
			else return false;
		} else {
			return false;
		}
	}

//	public boolean createDeliveryPersonInDatabase_old(
//			String name,
//			String username,
//			String password,
//			String phoneNumber,
//			String address,
//			String assignedRegionIds
//	) throws Exception {
//		String sqlStr = "";
//		int result = 0;
//
//		if (name != "" && username != "" && password != "" && phoneNumber != "" && address != "" && assignedRegionIds != "") {
//			sqlStr = "{ call create_delivery_person(?, ?, ?, ?, ?, ?, ?) }";
//			cStmt = con.prepareCall(sqlStr);
//			cStmt.setString("name_param", name);
//			cStmt.setString("username_param", username);
//			cStmt.setString("password_param", password);
//			cStmt.setString("phoneNumber_param", phoneNumber);
//			cStmt.setString("address_param", address);
//			cStmt.setString("assignedRegionIds_param", assignedRegionIds);
//			cStmt.registerOutParameter("update_count", Types.INTEGER);
//			cStmt.execute();
//			result = cStmt.getInt("update_count");
//			
//			if (result >= 1) 
//				return true
//			; else 
//				return false
//			;
//			
//		} else {
//			return false;
//		}
//	}
//	
	public boolean createDeliveryPersonInDatabase(
			String name,
			String username,
			String password,
			String phoneNumber,
			String address,
			String assignedRegions
	) throws Exception {
		String sqlStr = "";
		int result = 0;

		if (name != "" && username != "" && password != "" && phoneNumber != "" && address != "" && assignedRegions != "") {
			sqlStr = "" +
					"insert into delivery_persons (" + 
					"		name," + 
					"        username," + 
					"        password," + 
					"        phone_number," + 
					"        address," + 
					"        assigned_regions" + 
					"    ) values (" + 
					"		'"+ name + "'," + 
					"        '"+ username + "'," + 
					"        '"+ password + "'," + 
					"        '"+ phoneNumber + "'," + 
					"        '"+ address + "'," + 
					"        '"+ assignedRegions + "'" +
					"    );"
			;
			
			result = stmt.executeUpdate(sqlStr);
			
			if (result >= 1) 
				return true
			; else 
				return false
			;
			
		} else {
			return false;
		}
	}
	
//	public boolean createGenreInDatabase(String genreName) throws Exception {
//		String sqlStr = "";
//		int result = 0;
//		
//		if (genreName != "") {
//			sqlStr = "" +
//					"insert into genres (" + 
//					"  genre_name" +
//					") values (" + 
//					"  '" + genreName + "'" +
//					");"
//			;
//			
//			result = stmt.executeUpdate(sqlStr);
//			
//			if (result >= 1) return true;
//			else return false;
//		} else {
//			return false;
//		}
//	}
	
	public boolean createPublicationInDatabase(
			String publicationName,
			String publicationDate,
			double cost,
			String publicationFrequency,
			String genre
	) throws Exception {
		String sqlStr = "";
		int result = 0;
		
		if (publicationName != "" && publicationDate != "" && cost > 0 && cost <= 1000 && (publicationFrequency == "daily" || publicationFrequency == "weekly" || publicationFrequency == "monthly") && genre != "") {
			sqlStr = "" +
					"insert into publications (" +
					"  publication_name," + 
					"  publication_date," + 
					"  cost," + 
					"  publication_frequency," + 
					"  genre" + 
					") values (" + 
					"  '" + publicationName + "'," +
					"  '" + publicationDate + "'," +
					"  '" + cost + "'," +
					"  '" + publicationFrequency + "'," +
					"  '" + genre + "'" +
					");"
			;
			
			result = stmt.executeUpdate(sqlStr);
			
			if (result >= 1) return true;
			else return false;
		} else {
			return false;
		}
	}
}
