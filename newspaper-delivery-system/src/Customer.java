public class Customer {

	String[] invalidChars = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "!", "'", "�", "$", "%", "^", "&", "*",
			"(", ")", "-", "_", "+", "=", "�", "`", "[", "]", "{", "}", "#", "@", "\\", "/", "|", "<", ">", ",", ".",
			"~", "\"", "?" };

	String[] invalidCharsAddress = { "!", "'", "�", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "�", "`",
			"[", "]", "{", "}", "#", "@", "\\", "|", "<", ">", "~", "\"", "?" };

	String[] validNumber = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

	String lastName;
	String otherNames;
	String dateOfBirth;
	String address;
	String region;
	String deliveryFrequency;
	String phoneNumber;
	
	DatabaseConnector dbc;


	 public Customer(DatabaseConnector dbobj) {
		 dbc = dbobj;
	 }

	boolean validateLastName(String lastName) {
		boolean result = true;

		for (int i = 0; i < invalidChars.length; i++) {
			if (lastName.indexOf(invalidChars[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;

	}

	boolean validateOtherNames(String otherNames) {
		boolean result = true;

		for (int i = 0; i < invalidChars.length; i++) {
			if (otherNames.indexOf(invalidChars[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;

	}

	boolean validateDOB(String dateOfBirth) {
		boolean result = true;

		if (dateOfBirth.matches("\\d{2}-\\d{2}-\\d{2}")) {
			result = true;
		} else {
			result = false;
		}

		return result;

	}

	boolean validateAddress(String address) {
		boolean result = true;

		for (int i = 0; i < invalidCharsAddress.length; i++) {
			if (address.indexOf(invalidCharsAddress[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;

	}

	boolean validateRegion(String region) {
		boolean result = true;

		if (region == "") {
			result = false;
		}

		return result;

	}

	boolean validateDeliveryFrequency(String deliveryFrequency) {
		boolean result = true;

		if (deliveryFrequency == "0") {
			result = false;
		}

		return result;

	}

	boolean validatePhoneNumber(String phoneNumber) {
		for (int i = 0; i < phoneNumber.length(); i++) {
			for (int j = 0; j < validNumber.length; j++) {
				if (phoneNumber.substring(i, i + 1).equals(validNumber[j])) {
					break;
				} else if ((j + 1) == validNumber.length) {
					return false;
				}
			}
		}
		
		return true;
	}

	public String createCustomer(String lastName, String otherNames, String dateOfBirth, String address,String region,
			String deliveryFrequency, String phoneNumber) throws Exception{
		String result = "ok";
		boolean vr1 = validateLastName(lastName);
		boolean vr2 = validateOtherNames(otherNames);
		boolean vr3 = validateDOB(dateOfBirth);
		boolean vr4 = validateAddress(address);
		boolean vr5 = validateRegion(region);
		boolean vr6 = validateDeliveryFrequency(deliveryFrequency);
		boolean vr7 = validatePhoneNumber(phoneNumber);

		if (vr1 == false) {
			result = "Incorrect Last Name";
		} else if (vr2 == false) {
			result = "Incorrect Other Name";
		} else if (vr3 == false) {
			result = "Incorrect DOB";
		} else if (vr4 == false) {
			result = "Incorrect Address";
		} else if (vr5 == false) {
			result = "Incorrect Region";
		} else if (vr6 == false) {
			result = "Incorrect Delivery Freq";
		} else if (vr7 == false) {
			result = "Incorrect Phone Number";
		} else if ((vr1 == true) && (vr2 == true) && (vr3 == true) && (vr4 == true) && (vr5 == true) && (vr6 == true)
				&& (vr7 == true)) {
			result = dbc.createCustomerInDatabase(lastName, otherNames, dateOfBirth, phoneNumber, address, region,
					deliveryFrequency) ? "ok" : "fail";
		}
		return result;
	}
}


