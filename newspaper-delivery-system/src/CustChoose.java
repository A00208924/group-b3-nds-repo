import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CustChoose extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustChoose frame = new CustChoose();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}*/
	public static void NewScreen4(DatabaseConnector dbobj) {
	EventQueue.invokeLater(new Runnable() {
		public void run() {
			try {
				CustChoose frame = new CustChoose(dbobj);
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	});
	}

	/**
	 * Create the frame.
	 */
	public CustChoose(DatabaseConnector dbobj) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 378);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Add New Customer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				Custform nw=new Custform(dbobj);
				nw.NewScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
		});
		btnNewButton.setFont(new Font("����", Font.PLAIN, 16));
		btnNewButton.setBounds(119, 45, 191, 67);
		contentPane.add(btnNewButton);
		
		JButton btnSearchCustomer = new JButton("Search Customer");
		btnSearchCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				CustSearchForm nw=new CustSearchForm();
				nw.NewScreen8();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
		});
		btnSearchCustomer.setFont(new Font("����", Font.PLAIN, 16));
		btnSearchCustomer.setBounds(119, 153, 191, 67);
		contentPane.add(btnSearchCustomer);
		
		JButton btnNewButton_1 = new JButton("BACK");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				choosepage nw=new choosepage();
				nw.NewScreen3();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
			
		});
		btnNewButton_1.setFont(new Font("����", Font.PLAIN, 16));
		btnNewButton_1.setBounds(157, 270, 106, 36);
		contentPane.add(btnNewButton_1);
	}

}
